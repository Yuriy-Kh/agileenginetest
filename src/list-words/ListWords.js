import React from 'react';
import './ListWords.css';

const ListWords = ({ showList, listOfWords, position, handleClickList }) =>
  showList && (
    <div
      className="list-words-wrap"
      style={{ top: position.top + position.height, left: position.left }}
    >
      {listOfWords && listOfWords[0] !== 'Sorry, no synonyms' ? (
        <ul className="list-words" onMouseDown={handleClickList}>
          {listOfWords.map((word, index) => <li key={index}>{word}</li>)}
        </ul>
      ) : (
        <span className="no-result">{listOfWords[0]}</span>
      )}
    </div>
  );
export default ListWords;
