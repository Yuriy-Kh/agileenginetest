import React from 'react';
import './FileZone.css';

const createMarkup = text => {
  return { __html: text };
};
const FileZone = ({ content, checkStatusWord, handleContext }) => (
  <div id="file-zone">
    <div
      id="file"
      onDoubleClick={checkStatusWord}
      onContextMenu={handleContext}
      contentEditable="true"
      dangerouslySetInnerHTML={createMarkup(content)}
    />
  </div>
);

export default FileZone;
