import React, { Component } from 'react';
import './App.css';
import ControlPanel from './control-panel/ControlPanel';
import FileZone from './file-zone/FileZone';
import ListWords from './list-words/ListWords';
import getMockText from './text.service';

class App extends Component {
  state = {
    text: '',
    bold: false,
    italic: false,
    underline: false,
    synonyms: [],
    isShowContext: false,
    contextPosition: {},
  };
  getText = () => {
    getMockText()
      .then(result => {
        this.setState({ text: result });
      })
      .catch(reason => console.error(reason));
  };
  checkActiveButtons = type => {
    if (type === 'b') {
      this.setState({ bold: true });
    }
    if (type === 'i') {
      this.setState({ italic: true });
    }
    if (type === 'u') {
      this.setState({ underline: true });
    }
  };
  checkParentsButtonNodes = parent => {
    this.checkActiveButtons(parent.localName);
    if (parent.parentNode.localName !== 'div') {
      this.checkParentsButtonNodes(parent.parentNode);
    }
  };
  checkStatusWord = event => {
    const el = event.target;
    this.checkActiveButtons(el.localName);
    this.checkParentsButtonNodes(el.parentNode);
    this.setState({ synonyms: [] });
  };
  handleResetButtonsStatus = e => {
    if (e.target.type !== 'button' && e.target.parentNode.type !== 'button') {
      this.setState({ bold: false, italic: false, underline: false });
    }
    this.setState({ isShowContext: false });
  };
  toggleButtonStatus(e) {
    if (window.getSelection().toString().length !== 0) {
      this.setState(prevState => ({ [e]: !prevState[e] }));
    }
  }
  execCommand = (command, arg) => {
    this.toggleButtonStatus(command);
    document.execCommand(command, false, arg);
  };
  findSynonyms = e => {
    e.preventDefault();
    const word = window.getSelection().toString();
    word !== ''
      ? this.setState({ isShowContext: true })
      : this.setState({ isShowContext: false });

    this.setState({
      contextPosition: window
        .getSelection()
        .getRangeAt(0)
        .getBoundingClientRect(),
    });
    fetch(`http://api.datamuse.com/words?rel_syn=${word}`, {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(json => {
        json.length !== 0
          ? this.setState({ synonyms: json.map(item => item.word) })
          : this.setState({ synonyms: ['Sorry, no synonyms'] });
      })
      .catch(reason => console.error(reason));
  };
  addNewWord = e => {
    const synonym = `${e.target.innerText} `;
    const el = window.getSelection().getRangeAt(0);
    el.deleteContents();
    el.insertNode(document.createTextNode(synonym));
  };
  componentDidMount() {
    this.getText();
  }
  render() {
    const { text, synonyms, isShowContext, contextPosition } = this.state;
    return (
      <div className="App" onClick={this.handleResetButtonsStatus}>
        <header>
          <span>Simple Text Editor</span>
        </header>
        <main>
          <ControlPanel
            selectedText={this.execCommand}
            buttonStatus={this.state}
          />
          <FileZone
            content={text}
            checkStatusWord={this.checkStatusWord}
            handleContext={this.findSynonyms}
          />
          <ListWords
            showList={isShowContext}
            listOfWords={synonyms}
            position={contextPosition}
            handleClickList={this.addNewWord}
          />
        </main>
      </div>
    );
  }
}

export default App;
