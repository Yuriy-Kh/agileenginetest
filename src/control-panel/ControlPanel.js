import React from 'react';
import './ControlPanel.css';

const ControlPanel = ({ buttonStatus, selectedText }) => (
  <div id="control-panel">
    <div id="format-actions">
      <button
        className={`format-action ${buttonStatus.bold ? 'active' : ''}`}
        type="button"
        onClick={(...args) => selectedText('bold', ...args)}
      >
        <b>B</b>
      </button>
      <button
        className={`format-action ${buttonStatus.italic ? 'active' : ''}`}
        type="button"
        onClick={(...args) => selectedText('italic', ...args)}
      >
        <i>I</i>
      </button>
      <button
        className={`format-action ${buttonStatus.underline ? 'active' : ''}`}
        type="button"
        onClick={(...args) => selectedText('underline', ...args)}
      >
        <u>U</u>
      </button>
    </div>
  </div>
);

export default ControlPanel;
