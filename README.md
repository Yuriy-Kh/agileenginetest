# Simple text editor

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Initial setup

Run `npm install` in order to setup application

## Development server

Run `npm start` for a dev server.

## Notes

* Select a word by double-clicking and adding styles in the control panel
* Select a word by double-clicking and right-clicking select a synonym to replace the word.
